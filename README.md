## Data Infrastructure

The [handbook](https://about.gitlab.com/handbook/business-ops/data-team/data-infrastructure/)  is the single source of truth for all of our documentation. 

### Moved images

For a complete git history of this image visit the following repository: https://gitlab.com/gitlab-data/data-image/

### License

This code is distributed under the MIT license, please see the [LICENSE](LICENSE) file.
